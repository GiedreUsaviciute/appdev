package giedre.tabletest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Giedre on 12-Dec-16.
 */

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.MyExamHolder>{

    private LayoutInflater inflater;
    ArrayList <ExamView> data = new ArrayList<>();
    private Context context;

    public ExamAdapter(Context context, ArrayList<ExamView> data){
        inflater = LayoutInflater.from(context);
        this.context = context;
        setDataList(data);
    }

    @Override
    public MyExamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row_exam,parent,false);
        MyExamHolder holder = new MyExamHolder(view);
        return holder;
    }

    public void setDataList(ArrayList<ExamView> data){
        this.data = data;
        notifyItemChanged(0,data.size());
    }

    public void delete(int position){
        data.remove(position);

        ExamView item = new ExamView();
        String label;
        item = data.get(position);
        label = item.getLabel();
        SqlDatabaseHelper helper;
        helper = new SqlDatabaseHelper(context);
        helper.removeNote(label);

        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyExamHolder holder, int position) {
        ExamView current = data.get(position);
        holder.label.setText(current.label);
        holder.date.setText(current.date);
        holder.time.setText(current.time);
        holder.timeLeft.setText(current.timeLeft);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyExamHolder extends RecyclerView.ViewHolder{

        TextView label;
        TextView date;
        TextView time;
        TextView location;
        TextView timeLeft;

        public MyExamHolder(View itemView) {
            super(itemView);

            label = (TextView) itemView.findViewById(R.id.examLabel);
            date = (TextView) itemView.findViewById(R.id.examDate);
            time = (TextView) itemView.findViewById(R.id.examTime);
            location = (TextView) itemView.findViewById(R.id.examLocation);
            timeLeft = (TextView) itemView.findViewById(R.id.examTimeLeft);

        }
    }
}
