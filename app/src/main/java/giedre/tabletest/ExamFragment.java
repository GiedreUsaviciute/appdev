package giedre.tabletest;

import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.TimePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;


public class ExamFragment extends Fragment {

    SqlDatabaseHelper helper;
    private RecyclerView recyclerView;
    private ExamAdapter adapter;
    FloatingActionButton fab;
    ArrayList<ExamView> dataFromSql2 = new ArrayList<>();
    TextView timeView;

    public ExamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_exam, container, false);
        helper = new SqlDatabaseHelper(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerExam);

        adapter = new ExamAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        fab = (FloatingActionButton) view.findViewById(R.id.buttonAddExam);//change into exam button (floating)
        timeView = (TextView) view.findViewById(R.id.examTime);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
                adapter.notifyItemChanged(0, dataFromSql2.size());

            }


        });

        return view;
    }

    public ArrayList <ExamView> getData(){

        dataFromSql2 = helper.getExamData();//get axam data from sql
        recyclerView.invalidate();
        return dataFromSql2;

    }

    private void showDialog() {

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.add_exam);
        final EditText label = (EditText) dialog.findViewById(R.id.addExamLabel);
        final EditText location = (EditText) dialog.findViewById(R.id.addExamLocation);
        timeView = (TextView) dialog.findViewById(R.id.addExamTime);
        final TextView date = (TextView) dialog.findViewById(R.id.addExamDate);
        final Button add = (Button) dialog.findViewById(R.id.saveExam) ;
        Button cancel = (Button) dialog.findViewById(R.id.cancelExam);
        String returnedTime;
        timeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getChildFragmentManager(), "timePicker");
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getChildFragmentManager(), "datePicker");
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String strLabel = label.getText().toString();
                String strLocation = location.getText().toString();
                String strTime = timeView.getText().toString();
                String strDate = date.getText().toString();

                ExamView exam = new ExamView();
                exam.setLabel(strLabel);
                exam.setLocation(strLocation);
                exam.setTime(strTime);
                exam.setDate(strDate);

                try{
                    int space;
                    space = helper.insertExam(exam);
                    adapter.notifyItemInserted(space);
                    adapter.notifyItemRangeChanged(space, space+1);
                    adapter.notifyDataSetChanged();

                    timeView.setText(exam.getTime());

                    Fragment current = getFragmentManager().findFragmentByTag("EXAMTAG");
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.detach(current);
                    fragmentTransaction.attach(current);
                    fragmentTransaction.commit();
                }catch (Exception e){
                    Toast.makeText(v.getContext(),"unsuccessful", Toast.LENGTH_SHORT);
                }
                dialog.dismiss();
            }
        });

        dialog.show();


    }



}
