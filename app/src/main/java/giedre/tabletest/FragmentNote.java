package giedre.tabletest;


import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.EOFException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNote extends Fragment {

    SqlDatabaseHelper helper;
    private RecyclerView recyclerView;
    private ViewAdapter adapter;
    FloatingActionButton fab;
    String stringPopLabel;
    String stringPopText;
    int noteSize;
    //ArrayList<NoteView> data = new ArrayList<>();
    ArrayList<NoteView> dataFromSql = new ArrayList<>();

    public FragmentNote() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_note, container, false);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        int defSize = 100;
        noteSize = pref.getInt("note_size",defSize);

        helper = new SqlDatabaseHelper(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);

        adapter = new ViewAdapter(getActivity(),getData());
        adapter.notifyItemChanged(0,dataFromSql.size());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);


        fab = (FloatingActionButton)view.findViewById(R.id.buttonAddNote);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
                adapter.notifyItemChanged(0,dataFromSql.size());


            }
        });



        return view;
    }
    public ArrayList<NoteView> getData(){

        dataFromSql = helper.getNoteData();
        recyclerView.invalidate();
        Log.d("cia grazino", "tusciaaaa");

        return dataFromSql;
    }
    private void showDialog(){

        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.add_note);
        Button cancel = (Button) dialog.findViewById(R.id.cancelNote);
        Button save = (Button) dialog.findViewById(R.id.SaveNote);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText popLabel = (EditText) dialog.findViewById(R.id.noteLabel);
                EditText popText = (EditText) dialog.findViewById(R.id.noteText);
                popText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(noteSize)});
                stringPopLabel = popLabel.getText().toString();
                stringPopText =  popText.getText().toString();
                int image = R.drawable.logo;


                NoteView note = new NoteView();
                note.setIconId(image);
                note.setTitle(stringPopLabel);
                note.setText(stringPopText);
                try {
                    int space;
                    space = helper.insertNote(note);
                    Toast.makeText(v.getContext(), "Registry succesful", Toast.LENGTH_SHORT).show();
                    adapter.notifyItemInserted(space);
                    adapter.insert(space);
                    adapter.notifyItemRangeChanged(space, space+1);
                    adapter.notifyDataSetChanged();

                    Fragment currentFragment = getFragmentManager().findFragmentByTag("NOTETAG");
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.detach(currentFragment);
                    fragmentTransaction.attach(currentFragment);
                    fragmentTransaction.commit();

                }catch (Exception e){
                    Toast.makeText(v.getContext(), "nope", Toast.LENGTH_SHORT).show();

                }



                dialog.dismiss();

            }
        });
        dialog.show();

    }

}
