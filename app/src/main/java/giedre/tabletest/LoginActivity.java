package giedre.tabletest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;


public class LoginActivity extends AppCompatActivity {

    SqlDatabaseHelper  helper= new SqlDatabaseHelper(this);

    public EditText lUsername;
    public EditText lPassword;
    public TextView linkRegister;
    Connection con;
    String un,pass,db,ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText lUsername = (EditText) findViewById(R.id.lUsername);
        final EditText lPassword = (EditText) findViewById(R.id.lPassword);
        final TextView linkRegister = (TextView) findViewById(R.id.linkRegister);

        Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String lUsernameString = lUsername.getText().toString();
                String lPasswordString = lPassword.getText().toString();

                // TODO: Fix this, crashing and stuff.

                if (!lPasswordString.equals("") && !lUsernameString.equals("")){
                    String password = helper.findPassword(lUsernameString);
                    Log.d("password is", password);
                    if (lPasswordString.equals(password.toString())){
                        Intent mainPage = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(mainPage);
                        lUsername.setText("");
                        lPassword.setText("");
                    }else{
                        Toast.makeText(LoginActivity.this, "Invalid login details", Toast.LENGTH_SHORT).show();
                    }
                }else{Toast.makeText(LoginActivity.this, "Invalid login details", Toast.LENGTH_SHORT).show();}


            }
        });



        linkRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });
    }
}
