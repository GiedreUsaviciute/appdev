package giedre.tabletest;

/**
 * Created by Giedre on 11-Dec-16.
 */

public class NoteView {

    int iconId;
    String title;
    String text;

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
