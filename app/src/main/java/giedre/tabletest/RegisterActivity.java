package giedre.tabletest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    SqlDatabaseHelper helper = new SqlDatabaseHelper(this);
    Spinner yearSpinner;
    public int yearSpinnerValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText rUsername = (EditText) findViewById(R.id.rUsername);
        final EditText rPassword1 = (EditText) findViewById(R.id.rPassword1);
        final EditText rPassword2 = (EditText) findViewById(R.id.rPassword2);
        final EditText rEmail = (EditText) findViewById(R.id.rEmail);
        final EditText rUniversity = (EditText) findViewById(R.id.rUniversity);
        final EditText rCourse = (EditText) findViewById(R.id.rCourse);

        final Button registerButton = (Button) findViewById(R.id.registerButton);




        yearSpinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,R.array.yearArray,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(adapter);
        yearSpinner.setOnItemSelectedListener(this);


    }
    public void signUpClicked(View v){

        final EditText rUsername = (EditText) findViewById(R.id.rUsername);
        final EditText rPassword1 = (EditText) findViewById(R.id.rPassword1);
        final EditText rPassword2 = (EditText) findViewById(R.id.rPassword2);
        final EditText rEmail = (EditText) findViewById(R.id.rEmail);
        final EditText rUniversity = (EditText) findViewById(R.id.rUniversity);
        final EditText rCourse = (EditText) findViewById(R.id.rCourse);


        String rUsernameString = rUsername.getText().toString();
        String rPassword1String = rPassword1.getText().toString();
        String rPassword2String = rPassword2.getText().toString();
        String rEmailString = rEmail.getText().toString();
        String rUniversityString = rUniversity.getText().toString();
        String rCourseString = rCourse.getText().toString();
        int rYear = yearSpinnerValue;

        if (rUsernameString.equals("")) {
            toaster("Please insert username");
        }else if (rPassword1String.equals("")){
            toaster("Please insert password");
        }else if (rPassword2String.equals("")){
            toaster("Please repeat your password");
        }else if (!rPassword1String.equals(rPassword2String)){
            toaster("Passwords do not match");
        }else if (rEmailString.equals("")){
            toaster("Please insert your email address");
        }else if (rUniversityString.equals("")){
            toaster("Please insert yout university");
        }else if (rCourseString.equals("")){
            toaster("Please insert your course");
        } else {

            User user = new User();
            user.setUsername(rUsernameString);
            user.setPassword(rPassword1String);
            user.setEmail(rEmailString);
            user.setUniversity(rUniversityString);
            user.setCourse(rCourseString);
            user.setYear(rYear);

            helper.insertUser(user);
            Toast.makeText(this, "Registry succesful", Toast.LENGTH_SHORT).show();
            Intent loginPage = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(loginPage);

        }



    }

    public void toaster(String msg){

        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();}


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView selectedYear = (TextView) view;
        Toast.makeText(this,"You selected " + selectedYear.getText().toString(), Toast.LENGTH_SHORT).show();
        yearSpinnerValue = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Button b = (Button) findViewById(R.id.registerButton);
        b.setEnabled(false);

    }
}
