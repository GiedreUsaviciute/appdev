package giedre.tabletest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.preference.PreferenceFragment;
import android.support.v7.preference.PreferenceFragmentCompat;

/**
 * Created by Giedre on 13-Dec-16.
 */

public class Settings extends PreferenceFragmentCompat {


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings_fragment);
    }
}
