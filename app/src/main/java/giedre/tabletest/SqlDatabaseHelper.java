package giedre.tabletest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Giedre on 13-Nov-16.
 */

public class SqlDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "Users";
    public static final String TABLE_NAME = "user_details";
    public static final String COL1_NAME = "user_ID";
    public static final String COL2_NAME = "user_name";
    public static final String COL3_NAME = "password";
    public static final String COL4_NAME = "email";
    public static final String COL5_NAME = "university";
    public static final String COL6_NAME = "course";
    public static final String COL7_NAME = "year";
    public static final String COL8_NAME = "lectureTableNo";
    public static final String COL9_NAME = "noteTableNo";

    //notes table
    public static final String NOTE_TABLE_NAME = "user_notes";
    //public static final String NOTE_COL0_NAME = "note_icon";
    public static final String NOTE_COL1_NAME = "note_id";
    public static final String NOTE_COL2_NAME = "note_label";
    public static final String NOTE_COL3_NAME = "note_text";
    //public static final String NOTE_COL4_NAME = "note_userID";
    SQLiteDatabase db;

    //exams table
    public static  final String EXAM_TABLE_NAME = "user_exams";
    public static final String EXAM_COL1_NAME = "exam_id";
    public static final String EXAM_COL2_NAME = "exam_label";
    public static final String EXAM_COL3_NAME = "exam_date";
    public static final String EXAM_COL4_NAME = "exam_time";
    public static final String EXAM_COL5_NAME = "exam_location";



    //SQLiteDatabase db;
    private static final String TABLE_CREATE = "create table " +TABLE_NAME + " ( " +
            COL1_NAME + " integer primary key not null, " + COL2_NAME + " TEXT not null, "
            + COL3_NAME + " TEXT not null, " + COL4_NAME + " TEXT NULL, " + COL5_NAME + " TEXT NULL, "
            + COL6_NAME + " TEXT NULL, " + COL7_NAME + " INTEGER NULL, " + COL8_NAME +
            " INTEGER NULL, " + COL9_NAME + " INTEGER NULL )";

    private static  final String TABLE_OF_NOTES = "create table " + NOTE_TABLE_NAME + " ( " +
            NOTE_COL1_NAME + " integer primary key not null, "
            + NOTE_COL2_NAME + " TEXT not null," +
            NOTE_COL3_NAME + " TEXT not null )";
    //NOTE_COL0_NAME+ " INTEGER not null, "

    private static  final String TABLE_OF_EXAMS = "create table " + EXAM_TABLE_NAME + " ( "+
            EXAM_COL1_NAME + " integer primary key not null, " +
            EXAM_COL2_NAME + " TEXT not null, " +
            EXAM_COL3_NAME + " TEXT not null, " +
            EXAM_COL4_NAME + " TEXT not null, " +
            EXAM_COL5_NAME + " TEXT not null )";


    public SqlDatabaseHelper(Context context) {
        super(context, DATABASE_NAME ,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_OF_NOTES);
        db.execSQL(TABLE_OF_EXAMS);
    }
    public  int insertExam(ExamView examView){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String query = "select * from " + EXAM_TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();
        values.put(EXAM_COL2_NAME,examView.getLabel());
        values.put(EXAM_COL3_NAME,examView.getDate());
        values.put(EXAM_COL4_NAME,examView.getTime());
        values.put(EXAM_COL5_NAME,examView.getLocation());

        long result = db.insert(EXAM_TABLE_NAME, null, values);
        if (result > 0){
            Log.d("dbhelper","inserted successfully");
        }else{
            Log.d("dbhelper","failed to insert");
        }

        db.close();
        return count;
    }
    public int insertNote(NoteView noteView){


        db = this.getWritableDatabase();
        Log.d("atiadare","sdadas");
        ContentValues values = new ContentValues();
        String query = "select * from " + NOTE_TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();
        values.put(NOTE_COL2_NAME,noteView.getTitle());
        values.put(NOTE_COL3_NAME,noteView.getText());

        long result = db.insert(NOTE_TABLE_NAME, null, values);
        if (result > 0){
            Log.d("dbhelper","inserted successfully");
        }else{
            Log.d("dbhelper","failed to insert");
        }
        //cursor.close();
        db.close();
        return count;
    }

    public void insertUser(User user){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String query = "select * from "+ TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        int count = cursor.getCount();

        values.put(COL1_NAME,count);
        values.put(COL2_NAME,user.getUsername());
        values.put(COL3_NAME,user.getPassword());
        values.put(COL4_NAME,user.getEmail());
        values.put(COL5_NAME,user.getUniversity());
        values.put(COL6_NAME,user.getCourse());
        values.put(COL7_NAME,user.getYear());

        long result = db.insert(TABLE_NAME, null, values);
        if (result > 0){
            Log.d("dbhelper","inserted successfully");
        }else{
            Log.d("dbhelper","failed to insert");
        }
        cursor.close();
        db.close();
    }
    public void removeNote(String label){
        SQLiteDatabase db = this.getWritableDatabase();
        String table = NOTE_TABLE_NAME;
        String clause = NOTE_COL2_NAME + " = ?";
        String[] args = new String[]{String.valueOf(label)};
        db.delete(table,clause,args);

    }
    public ArrayList<NoteView> getNoteData(){

        db = this.getReadableDatabase();
        Cursor cursor;
        String table = NOTE_TABLE_NAME;
        String[] columsToReturn = new String[]{NOTE_COL2_NAME,NOTE_COL3_NAME};
        cursor = db.query(table, columsToReturn,null,null,null,null,null);
        ArrayList<NoteView> notes = new ArrayList<>();
        int image = R.drawable.logo;
        cursor.moveToFirst();

        if (cursor.moveToFirst()){
            NoteView item = new NoteView();
            item.setIconId(image);
            item.setTitle(cursor.getString(cursor.getColumnIndex(NOTE_COL2_NAME)));
            item.setText(cursor.getString(cursor.getColumnIndex(NOTE_COL3_NAME)));
            notes.add(item);
            while(cursor.moveToNext()){
                NoteView newItem = new NoteView();
                newItem.setIconId(image);
                newItem.setTitle(cursor.getString(cursor.getColumnIndex(NOTE_COL2_NAME)));
                newItem.setText(cursor.getString(cursor.getColumnIndex(NOTE_COL3_NAME)));
                notes.add(newItem);
            }
        }else{
            NoteView item = new NoteView();
            item.setIconId(image);
            item.setTitle("Nope");
            item.setText("Not working");
            notes.add(item);
        }
        cursor.close();
        db.close();
        return notes;
    }
    public ArrayList<ExamView> getExamData(){
        //change database lvl
        db = this.getReadableDatabase();
        Cursor cursor;
        String table = EXAM_TABLE_NAME;
        String[] columnsToReturn = new String[]{EXAM_COL2_NAME,EXAM_COL3_NAME,EXAM_COL4_NAME,EXAM_COL5_NAME};
        cursor = db.query(table, columnsToReturn, null, null, null, null, null, null);
        ArrayList<ExamView> exams = new ArrayList<>();
        cursor.moveToFirst();

        if(cursor.moveToFirst()){
            ExamView  item = new ExamView();
            item.setLabel(cursor.getString(cursor.getColumnIndex(EXAM_COL2_NAME)));
            item.setDate(cursor.getString(cursor.getColumnIndex(EXAM_COL3_NAME)));
            item.setLocation(cursor.getString(cursor.getColumnIndex(EXAM_COL5_NAME)));
            item.setTime(cursor.getString(cursor.getColumnIndex(EXAM_COL4_NAME)));
            item.setTimeLeft("5");
            exams.add(item);
            while(cursor.moveToNext()){
                ExamView newItem = new ExamView();
                newItem.setLabel(cursor.getString(cursor.getColumnIndex(EXAM_COL2_NAME)));
                newItem.setDate(cursor.getString(cursor.getColumnIndex(EXAM_COL3_NAME)));
                newItem.setLocation(cursor.getString(cursor.getColumnIndex(EXAM_COL5_NAME)));
                newItem.setTime(cursor.getString(cursor.getColumnIndex(EXAM_COL4_NAME)));
                newItem.setTimeLeft("5");
                exams.add(newItem);
            }
        }
        cursor.close();
        db.close();
        return exams;

    }

    public String findPassword(String username){

        String user = username;
        //if ( this.db != null)
        db = this.getReadableDatabase();
            String table = TABLE_NAME;
            String[] columsToReturn = new String[]{COL2_NAME,COL3_NAME};
            String selection = COL2_NAME + " =?";
            String[] selectionArgs = {user};
            Cursor cursor = db.query(table, columsToReturn,selection,selectionArgs,null, null,null);
            cursor.moveToFirst();
            String passw = cursor.getString(cursor.getColumnIndex(COL3_NAME));
            Log.d("password",passw);
            cursor.close();
            db.close();
            return passw;


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NOTE_TABLE_NAME);
        onCreate(db);

    }
}
