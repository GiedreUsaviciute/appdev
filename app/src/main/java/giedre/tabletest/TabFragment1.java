package giedre.tabletest;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import static giedre.tabletest.R.id.fab;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment1 extends Fragment {

    FloatingActionButton addMondayButton;
    public TabFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
        addMondayButton = (FloatingActionButton) view.findViewById(R.id.mondayButtonAdd);

        addMondayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showDialog();
                //adapter.notifyItemChanged(0, dataFromSql2.size());
            }
        });

        return view;
    }



}
