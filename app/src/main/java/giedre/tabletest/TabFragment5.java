package giedre.tabletest;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment5 extends Fragment {

    FloatingActionButton addFridayButton;

    public TabFragment5() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_fragment5, container, false);
        addFridayButton = (FloatingActionButton) view.findViewById(R.id.fridayButtonAdd);

        addFridayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showDialog();
                //adapter.notifyItemChanged(0, dataFromSql2.size());
            }
        });

        return view;
    }
}


