package giedre.tabletest;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Giedre on 11-Dec-16.
 */

public class ViewAdapter extends RecyclerView.Adapter <ViewAdapter.MyViewHolder>{

    private LayoutInflater inflater;
    ArrayList<NoteView> data = new ArrayList<>();
    private Context context;

    public ViewAdapter(Context context, ArrayList<NoteView> data){
        inflater = LayoutInflater.from(context);
        this.context = context;
        setDataList(data);


    }
    public void setDataList(ArrayList<NoteView> data){
        this.data = data;
        notifyItemChanged(0,data.size());
    }
    public void insert(int position){
        notifyItemInserted(position);
    }

    public void delete(int position){
        data.remove(position);

        NoteView item = new NoteView();
        String label;
        item = data.get(position);
        label = item.getTitle();
        SqlDatabaseHelper helper;
        helper = new SqlDatabaseHelper(context);
        helper.removeNote(label);

        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = inflater.inflate(R.layout.custom_row_note,parent,false);
        MyViewHolder holder = new MyViewHolder(v);


        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        NoteView current = data.get(position);
        holder.title.setText(current.title);
        holder.text.setText(current.text);
        holder.image.setImageResource(current.iconId);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView title;
        ImageView image;

        TextView text;
        public MyViewHolder(View itemView){

            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //delete(getPosition());

                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage("Do you want to delete this note?").setTitle("Delete note");
                    builder.setCancelable(true);
                    builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            delete(getPosition());
                            dialog.dismiss();

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return true;
                }
            });
            title = (TextView) itemView.findViewById(R.id.noteListLabel);
            image = (ImageView) itemView.findViewById(R.id.noteListImage);
            text = (TextView) itemView.findViewById(R.id.noteListText);
        }


        @Override
        public void onClick(View v) {

                Toast.makeText(context, "Hold to delete", Toast.LENGTH_SHORT).show();
        }
    }
}
